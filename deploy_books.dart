import 'dart:io';
import 'dart:convert';

void main(List<String> args) async {
  final currentScriptPath = Platform.script.normalizePath().toFilePath();
  final cwd = currentScriptPath.substring(
      0, currentScriptPath.lastIndexOf(Platform.pathSeparator));
  final pdfDir = Directory("$cwd${Platform.pathSeparator}pdf");
  final List<String> books = [];
  await for (var pdf in pdfDir.list(followLinks: false)) {
    books.add(pdf.absolute.path.split(Platform.pathSeparator).last);
  }
  File booksJson = File(
      "$cwd${Platform.pathSeparator}json${Platform.pathSeparator}books.json");
  await booksJson.writeAsString(jsonEncode(books));
}
